package com.astrapay.sdk.astrapayui.helper

import android.annotation.SuppressLint
import android.content.Context
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout

import com.astrapay.sdk.astrapayui.R

class ButtonHelper {
    var labelPaddingLeft: Int = 0
    var labelPaddingTop: Int = 0
    var labelPaddingRight: Int = 0
    var labelPaddingBottom: Int = 0

    var leftIconPaddingLeft: Int = 0
    var leftIconPaddingTop: Int = 0
    var leftIconPaddingRight: Int = 0
    var leftIconPaddingBottom: Int = 0

    var btnPaddingLeft: Int = 0
    var btnPaddingTop: Int = 0
    var btnPaddingRight: Int = 0
    var btnPaddingBottom: Int = 0

    var btnFitPaddingLeft:Int = 0
    var btnFitPaddingTop:Int = 0
    var btnFitPaddingRight:Int = 0
    var btnFitPaddingBottom:Int = 0

    var labelButtonTextColor: Int = 0
    var labelButtonFont:Int = 0
    var leftColor: Int = 0
    var rightColor: Int = 0

    var modeLeftVisibility: Int= View.VISIBLE
    var modeRightVisibility: Int= View.VISIBLE
    var modeLableButtontVisibility: Int= View.VISIBLE

    var btnLayoutColor:Int = 0
    var progressBarBtnColor:Int = 0

    var btnLayoutIsEnable:Boolean = true

    var btnLayoutWidth:Int = LinearLayout.LayoutParams.WRAP_CONTENT
    var btnLayoutHeight:Int = LinearLayout.LayoutParams.WRAP_CONTENT

    var btnLayoutRightWidth:Int = LinearLayout.LayoutParams.WRAP_CONTENT
    var btnLayoutRightHeight:Int = LinearLayout.LayoutParams.WRAP_CONTENT

    var btnLayoutGravity:Int = 0

    val primary: String = "primary"
    val warning: String = "warning"
    val danger: String = "danger"
    val success: String = "success"
    val primaryWhite: String = "primaryWhite"
    val info: String = "info"
    val light: String = "light"

    val link: String = "linkButton"
    val nude: String = "nudeButton"
    val outline: String = "outlineButton"
    val fill: String = "fillButton"

    val leadingContent: String = "leading"
    val iconContent: String = "icon"
    val trailingContent: String = "trailing"
    val trailingAndLeadingContent: String = "trailingAndLeading"
    val textContent: String = "text"

    val smallSize: String = "small"
    val mediumSize: String = "medium"
    val largeSize: String = "large"

    val disableFalse: String = "false"
    val disableTrue: String = "true"

    val defaultWidth: String = "default"
    val fitDeviceWidth: String = "fitDevice"

    fun getWidth(widths:String, content:String, style: String){
        when (widths) {
            defaultWidth -> {
                btnLayoutWidth = LinearLayout.LayoutParams.WRAP_CONTENT
                btnLayoutHeight = LinearLayout.LayoutParams.WRAP_CONTENT
                btnLayoutRightWidth = LinearLayout.LayoutParams.WRAP_CONTENT
                btnLayoutRightHeight = LinearLayout.LayoutParams.WRAP_CONTENT
                btnLayoutGravity = Gravity.CENTER_VERTICAL
            }
            fitDeviceWidth -> {
                if (style == fill || style == nude || style == outline){
                    btnLayoutWidth = LinearLayout.LayoutParams.MATCH_PARENT
                    btnLayoutHeight = LinearLayout.LayoutParams.WRAP_CONTENT

                    btnFitPaddingLeft = 24
                    btnFitPaddingTop = 24
                    btnFitPaddingRight = 24
                    btnFitPaddingBottom = 24
                    if (content == leadingContent || content == textContent || content == iconContent) {
                        btnLayoutGravity = Gravity.CENTER

                    } else if (content == trailingContent || content == trailingAndLeadingContent) {
                        btnLayoutRightWidth = LinearLayout.LayoutParams.MATCH_PARENT
                        btnLayoutRightHeight = LinearLayout.LayoutParams.WRAP_CONTENT
                    }
                } else {
                    btnLayoutWidth = LinearLayout.LayoutParams.WRAP_CONTENT
                    btnLayoutHeight = LinearLayout.LayoutParams.WRAP_CONTENT
                    btnLayoutGravity = Gravity.CENTER_VERTICAL
                }
            }
        }
    }

    fun getDisable(disable:String, style: String, type: String, context: Context){
        when (disable){
            disableFalse -> {
                btnLayoutIsEnable = true

            }
            disableTrue -> {
                btnLayoutIsEnable = false
                if (style == fill) {
                    if (type == primaryWhite) {
                        labelButtonTextColor = context.resources.getColor(R.color.gray_400)
                        leftColor = context.resources.getColor(R.color.gray_400)
                        rightColor = context.resources.getColor(R.color.gray_400)
                    }
                } else if (style == nude || style == outline || style == link){
                    if (type == primary){
                        labelButtonTextColor = context.resources.getColor(R.color.blue_100)
                        leftColor = context.resources.getColor(R.color.blue_100)
                        rightColor = context.resources.getColor(R.color.blue_100)
                    } else if (type == danger){
                        labelButtonTextColor = context.resources.getColor(R.color.red_100)
                        leftColor = context.resources.getColor(R.color.red_100)
                        rightColor = context.resources.getColor(R.color.red_100)
                    } else if (type == warning){
                        labelButtonTextColor = context.resources.getColor(R.color.orange_brand_100)
                        leftColor = context.resources.getColor(R.color.orange_brand_100)
                        rightColor = context.resources.getColor(R.color.orange_brand_100)
                    } else if (type == success){
                        labelButtonTextColor = context.resources.getColor(R.color.green_100)
                        leftColor = context.resources.getColor(R.color.green_100)
                        rightColor = context.resources.getColor(R.color.green_100)
                    } else if (type == primaryWhite){
                        labelButtonTextColor = context.resources.getColor(R.color.gray_400)
                        leftColor = context.resources.getColor(R.color.gray_400)
                        rightColor = context.resources.getColor(R.color.gray_400)
                    } else if (type == info){
                        labelButtonTextColor = context.resources.getColor(R.color.cyan_100)
                        leftColor = context.resources.getColor(R.color.cyan_100)
                        rightColor = context.resources.getColor(R.color.cyan_100)
                    } else if (type == light){
                        labelButtonTextColor = context.resources.getColor(R.color.gray_300)
                        leftColor = context.resources.getColor(R.color.gray_300)
                        rightColor = context.resources.getColor(R.color.gray_300)
                    }
                }
            }
        }
    }

    fun getType (type:String,context:Context){
        when (type) {
            primary -> {
                labelButtonTextColor = context.resources.getColor(R.color.blue_500)
                leftColor = context.resources.getColor(R.color.blue_500)
                rightColor = context.resources.getColor(R.color.blue_500)
            }
            primaryWhite -> {
                labelButtonTextColor = context.resources.getColor(R.color.gray_50)
                leftColor = context.resources.getColor(R.color.gray_50)
                rightColor = context.resources.getColor(R.color.gray_50)
            }
            success -> {
                labelButtonTextColor = context.resources.getColor(R.color.green_500)
                leftColor = context.resources.getColor(R.color.green_500)
                rightColor = context.resources.getColor(R.color.green_500)
            }
            warning -> {
                labelButtonTextColor = context.resources.getColor(R.color.orange_brand_500)
                leftColor = context.resources.getColor(R.color.orange_brand_500)
                rightColor = context.resources.getColor(R.color.orange_brand_500)
            }
            danger -> {
                labelButtonTextColor = context.resources.getColor(R.color.red_500)
                leftColor = context.resources.getColor(R.color.red_500)
                rightColor = context.resources.getColor(R.color.red_500)
            }
            info -> {
                labelButtonTextColor = context.resources.getColor(R.color.cyan_500)
                leftColor = context.resources.getColor(R.color.cyan_500)
                rightColor = context.resources.getColor(R.color.cyan_500)
            }
            light -> {
                labelButtonTextColor = context.resources.getColor(R.color.gray_700)
                leftColor = context.resources.getColor(R.color.gray_700)
                rightColor = context.resources.getColor(R.color.gray_700)
            }
        }
    }

    fun getContent(content: String) {
        when (content) {
            trailingAndLeadingContent -> {
                labelPaddingLeft = 8
                labelPaddingTop = 0
                labelPaddingRight = 8
                labelPaddingBottom = 0
            }
            trailingContent -> {
                modeLeftVisibility = View.GONE
                labelPaddingLeft = 0
                labelPaddingTop = 0
                labelPaddingRight = 8
                labelPaddingBottom = 0
            }
            leadingContent -> {
                modeRightVisibility = View.GONE
                labelPaddingLeft = 8
                labelPaddingTop = 0
                labelPaddingRight = 0
                labelPaddingBottom = 0
            }
            textContent -> {
                modeLeftVisibility = View.GONE
                modeRightVisibility = View.GONE
                labelPaddingLeft = 0
                labelPaddingTop = 0
                labelPaddingRight = 0
                labelPaddingBottom = 0
            }
            iconContent -> {
                modeLableButtontVisibility = View.GONE
                modeRightVisibility = View.GONE
                leftIconPaddingLeft = 0
                leftIconPaddingTop = 0
                leftIconPaddingRight = 0
                leftIconPaddingBottom = 0
            }
        }
    }

    @SuppressLint("ResourceType")
    fun getStyle(style:String, context:Context, type: String){
        when (style) {
            fill -> {
                if (type == primaryWhite){
                    btnLayoutColor = R.drawable.ripple_fill_button_primary_white
                    progressBarBtnColor = R.drawable.progress_bar_button_primary
                    labelButtonTextColor = context.resources.getColor(R.color.blue_500)
                    leftColor = context.resources.getColor(R.color.blue_500)
                    rightColor = context.resources.getColor(R.color.blue_500)
                }else if (type == primary || type == danger || type == warning || type == success || type == info || type == light){
                    progressBarBtnColor = R.drawable.progress_bar_button_white
                    labelButtonTextColor = context.resources.getColor(R.color.gray_50)
                    leftColor = context.resources.getColor(R.color.gray_50)
                    rightColor = context.resources.getColor(R.color.gray_50)
                    if (type == primary){
                        btnLayoutColor = R.drawable.ripple_fill_button_primary
                    } else if (type == danger){
                        btnLayoutColor = R.drawable.ripple_fill_button_danger
                    } else if (type == warning){
                        btnLayoutColor = R.drawable.ripple_fill_button_warning
                    } else if (type == success){
                        btnLayoutColor = R.drawable.ripple_fill_button_success
                    } else if (type == info){
                        btnLayoutColor = R.drawable.ripple_fill_button_info
                    } else if (type == light){
                        btnLayoutColor = R.drawable.ripple_fill_button_light
                    }
                }
            }
            nude -> {
                if (type == primary){
                    progressBarBtnColor = R.drawable.progress_bar_button_primary
                } else if (type == warning){
                    progressBarBtnColor = R.drawable.progress_bar_button_warning
                } else if (type == danger){
                    progressBarBtnColor = R.drawable.progress_bar_button_danger
                } else if (type == success){
                    progressBarBtnColor = R.drawable.progress_bar_button_success
                } else if (type == primaryWhite){
                    progressBarBtnColor = R.drawable.progress_bar_button_white
                } else if (type == info){
                    progressBarBtnColor = R.drawable.progress_bar_button_info
                } else if (type == light){
                    progressBarBtnColor = R.drawable.progress_bar_button_light
                }
            }
            outline -> {
                if (type == primary){
                    btnLayoutColor = R.drawable.ripple_outline_button_primary
                    progressBarBtnColor = R.drawable.progress_bar_button_primary
                } else if (type == warning){
                    btnLayoutColor = R.drawable.ripple_outline_button_warning
                    progressBarBtnColor = R.drawable.progress_bar_button_warning
                } else if (type == danger){
                    btnLayoutColor = R.drawable.ripple_outline_button_danger
                    progressBarBtnColor = R.drawable.progress_bar_button_danger
                } else if (type == success){
                    btnLayoutColor = R.drawable.ripple_outline_button_success
                    progressBarBtnColor = R.drawable.progress_bar_button_success
                } else if (type == primaryWhite){
                    btnLayoutColor = R.drawable.ripple_outline_button_primary_white
                    progressBarBtnColor = R.drawable.progress_bar_button_white
                } else if (type == info){
                    btnLayoutColor = R.drawable.ripple_outline_button_info
                    progressBarBtnColor = R.drawable.progress_bar_button_info
                } else if (type == light){
                    btnLayoutColor = R.drawable.ripple_outline_button_light
                    progressBarBtnColor = R.drawable.progress_bar_button_light
                }
            }
            link -> {
                if (type == primary){
                    progressBarBtnColor = R.drawable.progress_bar_button_primary
                } else if (type == warning){
                    progressBarBtnColor = R.drawable.progress_bar_button_warning
                } else if (type == danger){
                    progressBarBtnColor = R.drawable.progress_bar_button_danger
                } else if (type == success){
                    progressBarBtnColor = R.drawable.progress_bar_button_success
                } else if (type == primaryWhite){
                    progressBarBtnColor = R.drawable.progress_bar_button_white
                } else if (type == info){
                    progressBarBtnColor = R.drawable.progress_bar_button_info
                } else if (type == light){
                    progressBarBtnColor = R.drawable.progress_bar_button_light
                }
            }
        }
    }

    fun getSize(size:String, style: String){
        when (size){
            smallSize -> {
                labelButtonFont = R.style.H8
                if (style == link){
                    btnPaddingLeft = 0
                    btnPaddingTop= 0
                    btnPaddingRight = 0
                    btnPaddingBottom = 0
                } else {
                    btnPaddingLeft = 16
                    btnPaddingTop= 7
                    btnPaddingRight = 16
                    btnPaddingBottom = 7
                }
            }
            mediumSize -> {
                labelButtonFont = R.style.H7
                if (style == link){
                    btnPaddingLeft = 0
                    btnPaddingTop= 0
                    btnPaddingRight = 0
                    btnPaddingBottom = 0
                } else {
                    btnPaddingLeft = 16
                    btnPaddingTop= 9
                    btnPaddingRight = 16
                    btnPaddingBottom = 9
                }
            }
            largeSize -> {
                labelButtonFont = R.style.H6
                if (style == link){
                    btnPaddingLeft = 0
                    btnPaddingTop= 0
                    btnPaddingRight = 0
                    btnPaddingBottom = 0
                } else {
                    btnPaddingLeft = 16
                    btnPaddingTop= 12
                    btnPaddingRight = 16
                    btnPaddingBottom = 12
                }
            }
        }
    }
}