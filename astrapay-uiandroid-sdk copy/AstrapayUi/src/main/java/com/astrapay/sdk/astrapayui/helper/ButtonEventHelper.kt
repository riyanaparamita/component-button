package com.astrapay.sdk.astrapayui.helper

import android.content.Context
import android.graphics.Paint
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import com.astrapay.sdk.astrapayui.R

class ButtonEventHelper {
    var progressBarBtnVisibility: Int = View.GONE
    var leftIconVisibility: Int = View.VISIBLE
    var rightIconVisibility: Int = View.VISIBLE

    var labelButtonTextColor: Int = 0
    var labelButtonUnderline:Int = 0

    var leftIconColor: Int = 0
    var rightIconColor:Int = 0

    var labelPaddingLeft: Int = 0
    var labelPaddingTop:Int = 0
    var labelPaddingRight:Int = 0
    var labelPaddingBottom:Int = 0

    val link: String = "linkButton"
    val nude: String = "nudeButton"
    val outline: String = "outlineButton"
    val fill: String = "fillButton"

    val primary: String = "primary"
    val warning: String = "warning"
    val danger: String = "danger"
    val success: String = "success"
    val primaryWhite: String = "primaryWhite"
    val info: String = "info"
    val light: String = "light"

    val leadingContent: String = "leading"
    val iconContent: String = "icon"
    val trailingContent: String = "trailing"
    val trailingAndLeadingContent: String = "trailingAndLeading"
    val textContent: String = "text"


    fun clickEvent(style:String, context:Context, type:String, content:String){
        if (progressBarBtnVisibility == View.GONE ) {
            progressBarBtnVisibility = View.VISIBLE
            leftIconVisibility = View.GONE
            labelPaddingLeft = 8
            labelPaddingTop = 0
            labelPaddingBottom = 0
            if (content == leadingContent || content == textContent || content == iconContent){
                rightIconVisibility = View.GONE
                labelPaddingRight = 0
            } else if (content == trailingContent || content == trailingAndLeadingContent){
                rightIconVisibility = View.VISIBLE
                labelPaddingRight = 8
            }

            if (style == link || style == nude || style == outline){
                if (type == primary){
                    labelButtonTextColor = context.resources.getColor(R.color.blue_600)
                    leftIconColor = context.resources.getColor(R.color.blue_600)
                    rightIconColor = context.resources.getColor(R.color.blue_600)
                }else if (type == warning) {
                    labelButtonTextColor = context.resources.getColor(R.color.orange_brand_600)
                    leftIconColor = context.resources.getColor(R.color.orange_brand_600)
                    rightIconColor = context.resources.getColor(R.color.orange_brand_600)
                } else if (type == danger){
                    labelButtonTextColor = context.resources.getColor(R.color.red_600)
                    leftIconColor = context.resources.getColor(R.color.red_600)
                    rightIconColor = context.resources.getColor(R.color.red_600)
                } else if (type == success){
                    labelButtonTextColor = context.resources.getColor(R.color.green_600)
                    leftIconColor = context.resources.getColor(R.color.green_600)
                    rightIconColor = context.resources.getColor(R.color.green_600)
                } else if (type == primaryWhite){
                    labelButtonTextColor = context.resources.getColor(R.color.gray_200)
                    leftIconColor = context.resources.getColor(R.color.gray_200)
                    rightIconColor = context.resources.getColor(R.color.gray_200)
                } else if (type == info){
                    labelButtonTextColor = context.resources.getColor(R.color.cyan_600)
                    leftIconColor = context.resources.getColor(R.color.cyan_600)
                    rightIconColor = context.resources.getColor(R.color.cyan_600)
                } else if (type == light){
                    labelButtonTextColor = context.resources.getColor(R.color.gray_800)
                    leftIconColor = context.resources.getColor(R.color.gray_800)
                    rightIconColor = context.resources.getColor(R.color.gray_800)
                }

                if (style == link){
                    labelButtonUnderline =  Paint.UNDERLINE_TEXT_FLAG
                }
            }else if (style == fill){
                if (type == primaryWhite){
                    labelButtonTextColor = context.resources.getColor(R.color.blue_500)
                    leftIconColor = context.resources.getColor(R.color.blue_500)
                    rightIconColor = context.resources.getColor(R.color.blue_500)
                }else {
                    labelButtonTextColor = context.resources.getColor(R.color.gray_50)
                    leftIconColor = context.resources.getColor(R.color.gray_50)
                    rightIconColor = context.resources.getColor(R.color.gray_50)
                }
            }
        }else if (progressBarBtnVisibility == View.VISIBLE){
            progressBarBtnVisibility = View.GONE
            if (content == leadingContent || content == iconContent){
                leftIconVisibility = View.VISIBLE
            } else if (content == trailingContent ) {
                rightIconVisibility = View.VISIBLE
                labelPaddingLeft = 0
                labelPaddingTop = 0
                labelPaddingRight = 8
                labelPaddingBottom = 0
            } else if (content == trailingAndLeadingContent){
                leftIconVisibility = View.VISIBLE
                rightIconVisibility = View.VISIBLE
                labelPaddingRight = 8
            } else if (content == textContent){
                labelPaddingLeft = 0
                labelPaddingTop = 0
                labelPaddingRight = 0
                labelPaddingBottom = 0
            }

            if (style == link || style == nude || style == outline){
                if (type == primary){
                    labelButtonTextColor = context.resources.getColor(R.color.blue_500)
                    leftIconColor = context.resources.getColor(R.color.blue_500)
                    rightIconColor = context.resources.getColor(R.color.blue_500)
                } else if (type == warning) {
                    labelButtonTextColor = context.resources.getColor(R.color.orange_brand_500)
                    leftIconColor = context.resources.getColor(R.color.orange_brand_500)
                    rightIconColor = context.resources.getColor(R.color.orange_brand_500)
                } else if (type == danger){
                    labelButtonTextColor = context.resources.getColor(R.color.red_500)
                    leftIconColor = context.resources.getColor(R.color.red_500)
                    rightIconColor = context.resources.getColor(R.color.red_500)
                } else if (type == success){
                    labelButtonTextColor = context.resources.getColor(R.color.green_500)
                    leftIconColor = context.resources.getColor(R.color.green_500)
                    rightIconColor = context.resources.getColor(R.color.green_500)
                } else if (type == primaryWhite){
                    labelButtonTextColor = context.resources.getColor(R.color.gray_50)
                    leftIconColor = context.resources.getColor(R.color.gray_50)
                    rightIconColor = context.resources.getColor(R.color.gray_50)
                } else if (type == info){
                    labelButtonTextColor = context.resources.getColor(R.color.cyan_500)
                    leftIconColor = context.resources.getColor(R.color.cyan_500)
                    rightIconColor = context.resources.getColor(R.color.cyan_500)
                } else if (type == light){
                    labelButtonTextColor = context.resources.getColor(R.color.gray_700)
                    leftIconColor = context.resources.getColor(R.color.gray_700)
                    rightIconColor = context.resources.getColor(R.color.gray_700)
                }

                if (style == link){
                    labelButtonUnderline = 0
                }
            }else if (style == fill){
                if (type == primaryWhite){
                    labelButtonTextColor = context.resources.getColor(R.color.blue_500)
                    leftIconColor = context.resources.getColor(R.color.blue_500)
                    rightIconColor = context.resources.getColor(R.color.blue_500)
                } else {
                    labelButtonTextColor = context.resources.getColor(R.color.gray_50)
                    leftIconColor = context.resources.getColor(R.color.gray_50)
                    rightIconColor = context.resources.getColor(R.color.gray_50)
                }
            }
        }
    }

}