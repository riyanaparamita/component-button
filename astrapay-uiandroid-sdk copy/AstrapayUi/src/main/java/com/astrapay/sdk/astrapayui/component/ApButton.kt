package com.astrapay.sdk.astrapayui.component

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.*
import com.astrapay.sdk.astrapayui.R
import android.graphics.drawable.Drawable
import android.os.Build
import android.widget.TextView
import android.widget.LinearLayout
import android.widget.FrameLayout

import androidx.annotation.RequiresApi
import com.astrapay.sdk.astrapayui.helper.ButtonEventHelper

import com.astrapay.sdk.astrapayui.helper.ButtonHelper

@RequiresApi(Build.VERSION_CODES.M)
@SuppressLint("ResourceAsColor")
class ApButton(context: Context, attributeSet: AttributeSet)
    : FrameLayout(context, attributeSet) {

    private var btnLabel: String
    private var content: String
    private  var drawableLeft : Drawable
    private var drawableRight : Drawable
    private var style: String
    private var type: String
    private var size: String
    private var disable: String
    private var widths: String
//    private var btnToast: String
    private val buttonHelper: ButtonHelper = ButtonHelper()
    private val buttonEventHelper: ButtonEventHelper = ButtonEventHelper()


    val scale = resources.displayMetrics.density

    fun toPx(dp:Int ): Int {
        dp.toFloat()
        val dpAsPixels = (dp * scale + 0.5f).toInt()
        return dpAsPixels
    }


    init {
        val typedArray = context.obtainStyledAttributes(attributeSet, R.styleable.ApButton, 0, 0)
        btnLabel = typedArray.getString(R.styleable.ApButton_btnLabel) ?: ""
        drawableLeft = typedArray.getDrawable(R.styleable.ApButton_btnLeftIcon) ?: resources.getDrawable(R.drawable.ic_scan)
        drawableRight = typedArray.getDrawable(R.styleable.ApButton_btnRightIcon) ?: resources.getDrawable(R.drawable.ic_down_chevron)
        content = typedArray.getString(R.styleable.ApButton_btnContent) ?: ""
        style = typedArray.getString(R.styleable.ApButton_btnStyle) ?: ""
        type = typedArray.getString(R.styleable.ApButton_btnType) ?: ""
        size = typedArray.getString(R.styleable.ApButton_btnSize) ?: ""
        disable = typedArray.getString(R.styleable.ApButton_btnDisable) ?: "false"
//        btnToast = typedArray.getString(R.styleable.ApButton_btnToast) ?: "false"
        widths = typedArray.getString(R.styleable.ApButton_btnWidth) ?: ""
        typedArray.recycle()

        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val rootView = inflater.inflate(R.layout.ap_button, null)
        val labelButton: TextView = rootView.findViewById(R.id.labelBtn)
        val left : ImageView = rootView.findViewById(R.id.leftIcon)
        val right : ImageView = rootView.findViewById(R.id.rightIcon)
        val btnLayout : LinearLayout = rootView.findViewById(R.id.linearLayoutBtn)
        val btnLayoutFitDevice : LinearLayout = rootView.findViewById(R.id.linearLayoutFitDevice)
        val btnLayoutRight : LinearLayout = rootView.findViewById(R.id.testing)
        val progressBarBtn : ProgressBar = rootView.findViewById(R.id.progressBarBtn)

//        //tesButtonToast
//        val ll = findViewById<LinearLayout>(R.id.toastLayout)
//        val layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
//        val layoutToast = layoutInflater.inflate(R.layout.ap_toast, ll)

        var disableSelectedColor: Int = R.color.gray_50
        var selectedProgressBar: Int = R.drawable.progress_bar_button_white
        var selectedFillButton: Int = R.drawable.ripple_fill_button_primary
        var selectedOutlineButton: Int = R.drawable.ripple_outline_button_primary

        //HelperImplementationcontent
        buttonHelper.getContent(content)
        labelButton.setPaddingRelative(toPx(buttonHelper.labelPaddingLeft),toPx(buttonHelper.labelPaddingTop),toPx(buttonHelper.labelPaddingRight),toPx(buttonHelper.labelPaddingBottom))
        left.setPaddingRelative(toPx(buttonHelper.leftIconPaddingLeft),toPx(buttonHelper.leftIconPaddingTop),toPx(buttonHelper.leftIconPaddingRight),toPx(buttonHelper.leftIconPaddingBottom))
        left.visibility = buttonHelper.modeLeftVisibility
        right.visibility = buttonHelper.modeRightVisibility
        labelButton.visibility = buttonHelper.modeLableButtontVisibility

        //HelperImplementationType
        buttonHelper.getType(type,context)
        labelButton.setTextColor(buttonHelper.labelButtonTextColor)
        left.setColorFilter(buttonHelper.leftColor)
        right.setColorFilter(buttonHelper.leftColor)

        //HelperImplementationStyle
        buttonHelper.getStyle(style,context,type)
        btnLayout.setBackgroundResource(buttonHelper.btnLayoutColor)
        progressBarBtn.setBackgroundResource(buttonHelper.progressBarBtnColor)
        labelButton.setTextColor(buttonHelper.labelButtonTextColor)
        left.setColorFilter(buttonHelper.leftColor)
        right.setColorFilter(buttonHelper.rightColor)

        //HelperImplementationSizd
        buttonHelper.getSize(size,style)
        btnLayout.setPaddingRelative(toPx(buttonHelper.btnPaddingLeft),toPx(buttonHelper.btnPaddingTop),toPx(buttonHelper.btnPaddingRight),toPx(buttonHelper.btnPaddingBottom))
        labelButton.setTextAppearance(buttonHelper.labelButtonFont)

        //HelperImplementationDisable
        buttonHelper.getDisable(disable,style,type,context)
        btnLayout?.isEnabled = buttonHelper.btnLayoutIsEnable
        labelButton.setTextColor(buttonHelper.labelButtonTextColor)
        left.setColorFilter(buttonHelper.leftColor)
        right.setColorFilter(buttonHelper.rightColor)

//        HelperImplementationForSetOnClickListener
        btnLayout.setOnClickListener {
//        //helperImplementationForBtnToast
//        buttonEventHelper.getBtnToast(context,btnToast,layoutToast)
        buttonEventHelper.clickEvent(style,context,type,content)
        progressBarBtn.visibility = buttonEventHelper.progressBarBtnVisibility
        left.visibility = buttonEventHelper.leftIconVisibility
        right.visibility = buttonEventHelper.rightIconVisibility
        labelButton.setPaintFlags(buttonEventHelper.labelButtonUnderline)
        labelButton.setTextColor(buttonEventHelper.labelButtonTextColor)
        left.setColorFilter(buttonEventHelper.leftIconColor)
        right.setColorFilter(buttonEventHelper.rightIconColor)
        labelButton.setPaddingRelative(toPx(buttonEventHelper.labelPaddingLeft), toPx(buttonEventHelper.labelPaddingTop), toPx(buttonEventHelper.labelPaddingRight),toPx(buttonEventHelper.labelPaddingBottom))
        }



        //HelperImplementationForWidth
        buttonHelper.getWidth(widths,content,style)
        btnLayout.layoutParams = LinearLayout.LayoutParams(buttonHelper.btnLayoutWidth,buttonHelper.btnLayoutHeight)
        btnLayout.setGravity(buttonHelper.btnLayoutGravity)
        btnLayoutRight.layoutParams = LinearLayout.LayoutParams(buttonHelper.btnLayoutRightWidth,buttonHelper.btnLayoutRightHeight)
        btnLayoutFitDevice.setPaddingRelative(toPx(buttonHelper.btnFitPaddingLeft), toPx(buttonHelper.btnFitPaddingTop),toPx(buttonHelper.btnFitPaddingRight),toPx(buttonHelper.btnFitPaddingBottom))

        labelButton.text = btnLabel
        left.setImageDrawable(drawableLeft)
        right.setImageDrawable(drawableRight)

        addView(rootView)
    }



}