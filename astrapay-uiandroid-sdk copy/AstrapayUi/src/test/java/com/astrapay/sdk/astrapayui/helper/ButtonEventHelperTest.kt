package com.astrapay.sdk.astrapayui.helper

import android.content.Context
import android.content.res.Resources
import android.graphics.Paint
import android.view.View
import com.astrapay.sdk.astrapayui.R
import com.google.common.truth.Truth
import junit.framework.TestCase
import org.mockito.Mockito
import org.mockito.Mockito.mock

class ButtonEventHelperTest : TestCase() {
    lateinit var buttonEventHelper: ButtonEventHelper
    lateinit var mockResources: Resources
    lateinit var mockContext: Context

    public override fun setUp() {
        super.setUp()
        buttonEventHelper = ButtonEventHelper()
        mockContext = mock(Context::class.java)
        mockResources = mock(Resources::class.java)
    }

    fun testClickEvent() {
        var style: String = ""
        var type: String = ""
        var content: String = ""

        buttonEventHelper.clickEvent(style,mockContext,type,content)

        if (buttonEventHelper.progressBarBtnVisibility == View.GONE ) {
            Truth.assertThat(buttonEventHelper.progressBarBtnVisibility).isEqualTo(View.VISIBLE)
            Truth.assertThat(buttonEventHelper.leftIconVisibility).isEqualTo(View.GONE)
            Truth.assertThat(buttonEventHelper.labelPaddingLeft).isEqualTo(8)
            Truth.assertThat(buttonEventHelper.labelPaddingTop).isEqualTo(0)
            Truth.assertThat(buttonEventHelper.labelPaddingBottom).isEqualTo(0)
            if (content == buttonEventHelper.leadingContent || content == buttonEventHelper.textContent || content == buttonEventHelper.iconContent){
                Truth.assertThat(buttonEventHelper.rightIconVisibility).isEqualTo(View.GONE)
                Truth.assertThat(buttonEventHelper.labelPaddingRight).isEqualTo(0)
            } else if (content == buttonEventHelper.trailingContent || content == buttonEventHelper.trailingAndLeadingContent){
                Truth.assertThat(buttonEventHelper.labelPaddingRight).isEqualTo(8)
            }

            if (style == buttonEventHelper.link || style == buttonEventHelper.nude|| style == buttonEventHelper.outline){
                if (type == buttonEventHelper.primary){
                    Mockito.`when`(mockContext.getResources()).thenReturn(mockResources)
                    Mockito.`when`(mockResources.getColor(R.color.blue_600)).thenReturn(R.color.blue_600)

                    Truth.assertThat(buttonEventHelper.labelButtonTextColor).isEqualTo(mockContext.resources.getColor(R.color.blue_600))
                    Truth.assertThat(buttonEventHelper.leftIconColor).isEqualTo(mockContext.resources.getColor(R.color.blue_600))
                    Truth.assertThat(buttonEventHelper.rightIconColor).isEqualTo(mockContext.resources.getColor(R.color.blue_600))
                }else if (type == buttonEventHelper.warning) {
                    Mockito.`when`(mockContext.getResources()).thenReturn(mockResources)
                    Mockito.`when`(mockResources.getColor(R.color.orange_brand_600)).thenReturn(R.color.orange_brand_600)

                    Truth.assertThat(buttonEventHelper.labelButtonTextColor).isEqualTo(mockContext.resources.getColor(R.color.orange_brand_600))
                    Truth.assertThat(buttonEventHelper.leftIconColor).isEqualTo(mockContext.resources.getColor(R.color.orange_brand_600))
                    Truth.assertThat(buttonEventHelper.rightIconColor).isEqualTo(mockContext.resources.getColor(R.color.orange_brand_600))
                }else if (type == buttonEventHelper.danger){
                    Mockito.`when`(mockContext.getResources()).thenReturn(mockResources)
                    Mockito.`when`(mockResources.getColor(R.color.red_600)).thenReturn(R.color.red_600)

                    Truth.assertThat(buttonEventHelper.labelButtonTextColor).isEqualTo(mockContext.resources.getColor(R.color.red_600))
                    Truth.assertThat(buttonEventHelper.leftIconColor).isEqualTo(mockContext.resources.getColor(R.color.red_600))
                    Truth.assertThat(buttonEventHelper.rightIconColor).isEqualTo(mockContext.resources.getColor(R.color.red_600))
                }else if (type == buttonEventHelper.success){
                    Mockito.`when`(mockContext.getResources()).thenReturn(mockResources)
                    Mockito.`when`(mockResources.getColor(R.color.green_600)).thenReturn(R.color.green_600)

                    Truth.assertThat(buttonEventHelper.labelButtonTextColor).isEqualTo(mockContext.resources.getColor(R.color.green_600))
                    Truth.assertThat(buttonEventHelper.leftIconColor).isEqualTo(mockContext.resources.getColor(R.color.green_600))
                    Truth.assertThat(buttonEventHelper.rightIconColor).isEqualTo(mockContext.resources.getColor(R.color.green_600))
                }else if (type == buttonEventHelper.primaryWhite){
                    Mockito.`when`(mockContext.getResources()).thenReturn(mockResources)
                    Mockito.`when`(mockResources.getColor(R.color.gray_200)).thenReturn(R.color.gray_200)

                    Truth.assertThat(buttonEventHelper.labelButtonTextColor).isEqualTo(mockContext.resources.getColor(R.color.gray_200))
                    Truth.assertThat(buttonEventHelper.leftIconColor).isEqualTo(mockContext.resources.getColor(R.color.gray_200))
                    Truth.assertThat(buttonEventHelper.rightIconColor).isEqualTo(mockContext.resources.getColor(R.color.gray_200))
                }else if (type == buttonEventHelper.info){
                    Mockito.`when`(mockContext.getResources()).thenReturn(mockResources)
                    Mockito.`when`(mockResources.getColor(R.color.cyan_600)).thenReturn(R.color.cyan_600)

                    Truth.assertThat(buttonEventHelper.labelButtonTextColor).isEqualTo(mockContext.resources.getColor(R.color.cyan_600))
                    Truth.assertThat(buttonEventHelper.leftIconColor).isEqualTo(mockContext.resources.getColor(R.color.cyan_600))
                    Truth.assertThat(buttonEventHelper.rightIconColor).isEqualTo(mockContext.resources.getColor(R.color.cyan_600))
                }else if (type == buttonEventHelper.light){
                    Mockito.`when`(mockContext.getResources()).thenReturn(mockResources)
                    Mockito.`when`(mockResources.getColor(R.color.gray_800)).thenReturn(R.color.gray_800)

                    Truth.assertThat(buttonEventHelper.labelButtonTextColor).isEqualTo(mockContext.resources.getColor(R.color.gray_800))
                    Truth.assertThat(buttonEventHelper.leftIconColor).isEqualTo(mockContext.resources.getColor(R.color.gray_800))
                    Truth.assertThat(buttonEventHelper.rightIconColor).isEqualTo(mockContext.resources.getColor(R.color.gray_800))
                }

                if (style == buttonEventHelper.link){
                    Truth.assertThat(buttonEventHelper.labelButtonTextColor).isEqualTo(Paint.UNDERLINE_TEXT_FLAG)
                }
            }else if (style == buttonEventHelper.fill){
                if (type == buttonEventHelper.primaryWhite){
                    Mockito.`when`(mockContext.getResources()).thenReturn(mockResources)
                    Mockito.`when`(mockResources.getColor(R.color.blue_500)).thenReturn(R.color.blue_500)

                    Truth.assertThat(buttonEventHelper.labelButtonTextColor).isEqualTo(mockContext.resources.getColor(R.color.blue_500))
                    Truth.assertThat(buttonEventHelper.leftIconColor).isEqualTo(mockContext.resources.getColor(R.color.blue_500))
                    Truth.assertThat(buttonEventHelper.rightIconColor).isEqualTo(mockContext.resources.getColor(R.color.blue_500))
                }else{
                    Mockito.`when`(mockContext.getResources()).thenReturn(mockResources)
                    Mockito.`when`(mockResources.getColor(R.color.gray_50)).thenReturn(R.color.gray_50)

                    Truth.assertThat(buttonEventHelper.labelButtonTextColor).isEqualTo(mockContext.resources.getColor(R.color.gray_50))
                    Truth.assertThat(buttonEventHelper.leftIconColor).isEqualTo(mockContext.resources.getColor(R.color.gray_50))
                    Truth.assertThat(buttonEventHelper.rightIconColor).isEqualTo(mockContext.resources.getColor(R.color.gray_50))
                }
            }
        }else if (buttonEventHelper.progressBarBtnVisibility == View.VISIBLE ) {
            if (content == buttonEventHelper.leadingContent || content == buttonEventHelper.iconContent){
                Truth.assertThat(buttonEventHelper.leftIconVisibility).isEqualTo(View.VISIBLE)
            }else if (content == buttonEventHelper.trailingContent ) {
                Truth.assertThat(buttonEventHelper.rightIconVisibility).isEqualTo(View.VISIBLE)
                Truth.assertThat(buttonEventHelper.labelPaddingLeft).isEqualTo(0)
                Truth.assertThat(buttonEventHelper.labelPaddingTop).isEqualTo(0)
                Truth.assertThat(buttonEventHelper.labelPaddingRight).isEqualTo(8)
                Truth.assertThat(buttonEventHelper.labelPaddingBottom).isEqualTo(0)
            }else if (content == buttonEventHelper.trailingAndLeadingContent){
                Truth.assertThat(buttonEventHelper.leftIconVisibility).isEqualTo(View.VISIBLE)
                Truth.assertThat(buttonEventHelper.rightIconVisibility).isEqualTo(View.VISIBLE)
            }else if (content == buttonEventHelper.textContent){
                Truth.assertThat(buttonEventHelper.rightIconVisibility).isEqualTo(View.VISIBLE)
                Truth.assertThat(buttonEventHelper.labelPaddingLeft).isEqualTo(0)
                Truth.assertThat(buttonEventHelper.labelPaddingTop).isEqualTo(0)
                Truth.assertThat(buttonEventHelper.labelPaddingRight).isEqualTo(0)
                Truth.assertThat(buttonEventHelper.labelPaddingBottom).isEqualTo(0)
            }

            if (style == buttonEventHelper.link || style == buttonEventHelper.nude || style == buttonEventHelper.nude){
                if (type == buttonEventHelper.primary){
                    Mockito.`when`(mockContext.getResources()).thenReturn(mockResources)
                    Mockito.`when`(mockResources.getColor(R.color.blue_500)).thenReturn(R.color.blue_500)

                    Truth.assertThat(buttonEventHelper.labelButtonTextColor).isEqualTo(mockContext.resources.getColor(R.color.blue_500))
                    Truth.assertThat(buttonEventHelper.leftIconColor).isEqualTo(mockContext.resources.getColor(R.color.blue_500))
                    Truth.assertThat(buttonEventHelper.rightIconColor).isEqualTo(mockContext.resources.getColor(R.color.blue_500))
                }else if (type == buttonEventHelper.warning) {
                    Mockito.`when`(mockContext.getResources()).thenReturn(mockResources)
                    Mockito.`when`(mockResources.getColor(R.color.orange_brand_500)).thenReturn(R.color.orange_brand_500)

                    Truth.assertThat(buttonEventHelper.labelButtonTextColor).isEqualTo(mockContext.resources.getColor(R.color.orange_brand_500))
                    Truth.assertThat(buttonEventHelper.leftIconColor).isEqualTo(mockContext.resources.getColor(R.color.orange_brand_500))
                    Truth.assertThat(buttonEventHelper.rightIconColor).isEqualTo(mockContext.resources.getColor(R.color.orange_brand_500))
                }else if (type == buttonEventHelper.danger){
                    Mockito.`when`(mockContext.getResources()).thenReturn(mockResources)
                    Mockito.`when`(mockResources.getColor(R.color.red_500)).thenReturn(R.color.red_500)

                    Truth.assertThat(buttonEventHelper.labelButtonTextColor).isEqualTo(mockContext.resources.getColor(R.color.red_500))
                    Truth.assertThat(buttonEventHelper.leftIconColor).isEqualTo(mockContext.resources.getColor(R.color.red_500))
                    Truth.assertThat(buttonEventHelper.rightIconColor).isEqualTo(mockContext.resources.getColor(R.color.red_500))
                }else if (type == buttonEventHelper.success){
                    Mockito.`when`(mockContext.getResources()).thenReturn(mockResources)
                    Mockito.`when`(mockResources.getColor(R.color.green_500)).thenReturn(R.color.green_500)

                    Truth.assertThat(buttonEventHelper.labelButtonTextColor).isEqualTo(mockContext.resources.getColor(R.color.green_500))
                    Truth.assertThat(buttonEventHelper.leftIconColor).isEqualTo(mockContext.resources.getColor(R.color.green_500))
                    Truth.assertThat(buttonEventHelper.rightIconColor).isEqualTo(mockContext.resources.getColor(R.color.green_500))
                }else if (type == buttonEventHelper.primaryWhite){
                    Mockito.`when`(mockContext.getResources()).thenReturn(mockResources)
                    Mockito.`when`(mockResources.getColor(R.color.gray_50)).thenReturn(R.color.gray_50)

                    Truth.assertThat(buttonEventHelper.labelButtonTextColor).isEqualTo(mockContext.resources.getColor(R.color.gray_50))
                    Truth.assertThat(buttonEventHelper.leftIconColor).isEqualTo(mockContext.resources.getColor(R.color.gray_50))
                    Truth.assertThat(buttonEventHelper.rightIconColor).isEqualTo(mockContext.resources.getColor(R.color.gray_50))
                }else if (type == buttonEventHelper.info){
                    Mockito.`when`(mockContext.getResources()).thenReturn(mockResources)
                    Mockito.`when`(mockResources.getColor(R.color.cyan_500)).thenReturn(R.color.cyan_500)

                    Truth.assertThat(buttonEventHelper.labelButtonTextColor).isEqualTo(mockContext.resources.getColor(R.color.cyan_500))
                    Truth.assertThat(buttonEventHelper.leftIconColor).isEqualTo(mockContext.resources.getColor(R.color.cyan_500))
                    Truth.assertThat(buttonEventHelper.rightIconColor).isEqualTo(mockContext.resources.getColor(R.color.cyan_500))
                }else if (type == buttonEventHelper.light){
                    Mockito.`when`(mockContext.getResources()).thenReturn(mockResources)
                    Mockito.`when`(mockResources.getColor(R.color.gray_700)).thenReturn(R.color.gray_700)

                    Truth.assertThat(buttonEventHelper.labelButtonTextColor).isEqualTo(mockContext.resources.getColor(R.color.gray_700))
                    Truth.assertThat(buttonEventHelper.leftIconColor).isEqualTo(mockContext.resources.getColor(R.color.gray_700))
                    Truth.assertThat(buttonEventHelper.rightIconColor).isEqualTo(mockContext.resources.getColor(R.color.gray_700))
                }

                if (style == buttonEventHelper.link){
                    Truth.assertThat(buttonEventHelper.labelButtonUnderline).isEqualTo(0)
                }
            }else if (style == buttonEventHelper.fill){
                if (type == buttonEventHelper.primaryWhite){
                    Mockito.`when`(mockContext.getResources()).thenReturn(mockResources)
                    Mockito.`when`(mockResources.getColor(R.color.blue_500)).thenReturn(R.color.blue_500)

                    Truth.assertThat(buttonEventHelper.labelButtonTextColor).isEqualTo(mockContext.resources.getColor(R.color.blue_500))
                    Truth.assertThat(buttonEventHelper.leftIconColor).isEqualTo(mockContext.resources.getColor(R.color.blue_500))
                    Truth.assertThat(buttonEventHelper.rightIconColor).isEqualTo(mockContext.resources.getColor(R.color.blue_500))
                }else{
                    Mockito.`when`(mockContext.getResources()).thenReturn(mockResources)
                    Mockito.`when`(mockResources.getColor(R.color.gray_50)).thenReturn(R.color.gray_50)

                    Truth.assertThat(buttonEventHelper.labelButtonTextColor).isEqualTo(mockContext.resources.getColor(R.color.gray_50))
                    Truth.assertThat(buttonEventHelper.leftIconColor).isEqualTo(mockContext.resources.getColor(R.color.gray_50))
                    Truth.assertThat(buttonEventHelper.rightIconColor).isEqualTo(mockContext.resources.getColor(R.color.gray_50))
                }
            }
        }
    }
}
