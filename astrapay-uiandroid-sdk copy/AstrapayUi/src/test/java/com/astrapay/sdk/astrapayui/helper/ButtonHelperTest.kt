package com.astrapay.sdk.astrapayui.helper


import android.view.View
import com.google.common.truth.Truth
import junit.framework.TestCase
import android.view.Gravity
import android.widget.LinearLayout
import com.astrapay.sdk.astrapayui.R
import android.content.Context;
import android.content.res.Resources
import org.mockito.Mockito
import org.mockito.Mockito.mock

class ButtonHelperTest() : TestCase() {
    lateinit var buttonHelper: ButtonHelper
    lateinit var mockResources: Resources
    lateinit var mockContext: Context

    public override fun setUp() {
        super.setUp()
        buttonHelper= ButtonHelper()
        mockContext = mock(Context::class.java)
        mockResources = mock(Resources::class.java)
    }

    //testForButtonMode
    fun testGetContentTrailingAndLeading() {
        var content: String = buttonHelper.trailingAndLeadingContent
        buttonHelper.getContent(content)
        Truth.assertThat(buttonHelper.modeLeftVisibility).isEqualTo(View.VISIBLE)
        Truth.assertThat(buttonHelper.modeRightVisibility).isEqualTo(View.VISIBLE)
        Truth.assertThat(buttonHelper.modeLableButtontVisibility).isEqualTo(View.VISIBLE)
        Truth.assertThat(buttonHelper.labelPaddingLeft).isEqualTo(8)
        Truth.assertThat(buttonHelper.labelPaddingTop).isEqualTo(0)
        Truth.assertThat(buttonHelper.labelPaddingRight).isEqualTo(8)
        Truth.assertThat(buttonHelper.labelPaddingBottom).isEqualTo(0)
    }

    fun testGetContentTrailing() {
        var content: String = buttonHelper.trailingContent
        buttonHelper.getContent(content)
        Truth.assertThat(buttonHelper.modeLeftVisibility).isEqualTo(View.GONE)
        Truth.assertThat(buttonHelper.modeRightVisibility).isEqualTo(View.VISIBLE)
        Truth.assertThat(buttonHelper.modeLableButtontVisibility).isEqualTo(View.VISIBLE)
        Truth.assertThat(buttonHelper.labelPaddingLeft).isEqualTo(0)
        Truth.assertThat(buttonHelper.labelPaddingTop).isEqualTo(0)
        Truth.assertThat(buttonHelper.labelPaddingRight).isEqualTo(8)
        Truth.assertThat(buttonHelper.labelPaddingBottom).isEqualTo(0)
    }

    fun testGetContentLeading() {
        var content: String = buttonHelper.leadingContent
        buttonHelper.getContent(content)
        Truth.assertThat(buttonHelper.modeLeftVisibility).isEqualTo(View.VISIBLE)
        Truth.assertThat(buttonHelper.modeRightVisibility).isEqualTo(View.GONE)
        Truth.assertThat(buttonHelper.modeLableButtontVisibility).isEqualTo(View.VISIBLE)
        Truth.assertThat(buttonHelper.labelPaddingLeft).isEqualTo(8)
        Truth.assertThat(buttonHelper.labelPaddingTop).isEqualTo(0)
        Truth.assertThat(buttonHelper.labelPaddingRight).isEqualTo(0)
        Truth.assertThat(buttonHelper.labelPaddingBottom).isEqualTo(0)
    }

    fun testGetContentText() {
        var content: String = buttonHelper.textContent
        buttonHelper.getContent(content)
        Truth.assertThat(buttonHelper.modeLeftVisibility).isEqualTo(View.GONE)
        Truth.assertThat(buttonHelper.modeRightVisibility).isEqualTo(View.GONE)
        Truth.assertThat(buttonHelper.modeLableButtontVisibility).isEqualTo(View.VISIBLE)
        Truth.assertThat(buttonHelper.labelPaddingLeft).isEqualTo(0)
        Truth.assertThat(buttonHelper.labelPaddingTop).isEqualTo(0)
        Truth.assertThat(buttonHelper.labelPaddingRight).isEqualTo(0)
        Truth.assertThat(buttonHelper.labelPaddingBottom).isEqualTo(0)
    }

    fun testGetContentIcon() {
        var content: String = buttonHelper.iconContent
        buttonHelper.getContent(content)
        Truth.assertThat(buttonHelper.modeLeftVisibility).isEqualTo(View.VISIBLE)
        Truth.assertThat(buttonHelper.modeRightVisibility).isEqualTo(View.GONE)
        Truth.assertThat(buttonHelper.modeLableButtontVisibility).isEqualTo(View.GONE)
        Truth.assertThat(buttonHelper.leftIconPaddingLeft).isEqualTo(0)
        Truth.assertThat(buttonHelper.leftIconPaddingTop).isEqualTo(0)
        Truth.assertThat(buttonHelper.leftIconPaddingRight).isEqualTo(0)
        Truth.assertThat(buttonHelper.leftIconPaddingBottom).isEqualTo(0)
    }

    //testForButtonWidth
    fun testGetWidthDefault() {
        var width: String = buttonHelper.defaultWidth
        var content: String = ""
        var style:String = ""
        buttonHelper.getWidth(width,content,style)
        Truth.assertThat(buttonHelper.btnLayoutWidth).isEqualTo(LinearLayout.LayoutParams.WRAP_CONTENT)
        Truth.assertThat(buttonHelper.btnLayoutHeight).isEqualTo(LinearLayout.LayoutParams.WRAP_CONTENT)
        Truth.assertThat(buttonHelper.btnLayoutRightHeight).isEqualTo(LinearLayout.LayoutParams.WRAP_CONTENT)
        Truth.assertThat(buttonHelper.btnLayoutRightWidth).isEqualTo(LinearLayout.LayoutParams.WRAP_CONTENT)
    }

    fun testGetWidthFitDevice() {
        var width: String = buttonHelper.fitDeviceWidth
        var content: String = ""
        var style:String = ""
        buttonHelper.getWidth(width,content,style)
        if (style == buttonHelper.fill|| style == buttonHelper.nude || style == buttonHelper.outline){
            Truth.assertThat(buttonHelper.btnLayoutWidth).isEqualTo(LinearLayout.LayoutParams.MATCH_PARENT)
            Truth.assertThat(buttonHelper.btnLayoutHeight).isEqualTo(LinearLayout.LayoutParams.WRAP_CONTENT)
            Truth.assertThat(buttonHelper.btnFitPaddingLeft).isEqualTo(24)
            Truth.assertThat(buttonHelper.btnFitPaddingTop).isEqualTo(24)
            Truth.assertThat(buttonHelper.btnFitPaddingRight).isEqualTo(24)
            Truth.assertThat(buttonHelper.btnFitPaddingBottom).isEqualTo(24)
            if (content == buttonHelper.leadingContent || content == buttonHelper.textContent || content == buttonHelper.iconContent) {
                Truth.assertThat(buttonHelper.btnLayoutGravity).isEqualTo(Gravity.CENTER)
            }else if (content == buttonHelper.trailingContent || content == buttonHelper.trailingAndLeadingContent) {
                Truth.assertThat(buttonHelper.btnLayoutRightWidth).isEqualTo(LinearLayout.LayoutParams.MATCH_PARENT)
                Truth.assertThat(buttonHelper.btnLayoutRightHeight).isEqualTo(LinearLayout.LayoutParams.WRAP_CONTENT)
            }
        }else{
            Truth.assertThat(buttonHelper.btnLayoutWidth).isEqualTo(LinearLayout.LayoutParams.WRAP_CONTENT)
            Truth.assertThat(buttonHelper.btnLayoutHeight).isEqualTo(LinearLayout.LayoutParams.WRAP_CONTENT)
            Truth.assertThat(buttonHelper.btnLayoutGravity).isEqualTo(Gravity.CENTER_VERTICAL)
        }
    }

    //tesForButtonSize
    fun testGetSizeSmall() {
        var size:String = buttonHelper.smallSize
        var style:String = ""

        buttonHelper.getSize(size,style)
        Truth.assertThat(buttonHelper.labelButtonFont).isEqualTo(R.style.H8)
        if (style == buttonHelper.link){
            Truth.assertThat(buttonHelper.btnPaddingLeft).isEqualTo(0)
            Truth.assertThat(buttonHelper.btnPaddingTop).isEqualTo(0)
            Truth.assertThat(buttonHelper.btnPaddingRight).isEqualTo(0)
            Truth.assertThat(buttonHelper.btnPaddingBottom).isEqualTo(0)
        } else {
            Truth.assertThat(buttonHelper.btnPaddingLeft).isEqualTo(16)
            Truth.assertThat(buttonHelper.btnPaddingTop).isEqualTo(7)
            Truth.assertThat(buttonHelper.btnPaddingRight).isEqualTo(16)
            Truth.assertThat(buttonHelper.btnPaddingBottom).isEqualTo(7)
        }
    }

    fun testGetSizeMedium() {
        var size:String = buttonHelper.mediumSize
        var style:String = ""

        buttonHelper.getSize(size,style)
        Truth.assertThat(buttonHelper.labelButtonFont).isEqualTo(R.style.H7)
        if (style == buttonHelper.link){
            Truth.assertThat(buttonHelper.btnPaddingLeft).isEqualTo(0)
            Truth.assertThat(buttonHelper.btnPaddingTop).isEqualTo(0)
            Truth.assertThat(buttonHelper.btnPaddingRight).isEqualTo(0)
            Truth.assertThat(buttonHelper.btnPaddingBottom).isEqualTo(0)
        } else {
            Truth.assertThat(buttonHelper.btnPaddingLeft).isEqualTo(16)
            Truth.assertThat(buttonHelper.btnPaddingTop).isEqualTo(9)
            Truth.assertThat(buttonHelper.btnPaddingRight).isEqualTo(16)
            Truth.assertThat(buttonHelper.btnPaddingBottom).isEqualTo(9)
        }
    }

    fun testGetSizeLarge() {
        var size:String = buttonHelper.largeSize
        var style:String = ""

        buttonHelper.getSize(size,style)
        Truth.assertThat(buttonHelper.labelButtonFont).isEqualTo(R.style.H6)
        if (style == buttonHelper.link){
            Truth.assertThat(buttonHelper.btnPaddingLeft).isEqualTo(0.0)
            Truth.assertThat(buttonHelper.btnPaddingTop).isEqualTo(0.0)
            Truth.assertThat(buttonHelper.btnPaddingRight).isEqualTo(0.0)
            Truth.assertThat(buttonHelper.btnPaddingBottom).isEqualTo(0.0)
        } else {
            Truth.assertThat(buttonHelper.btnPaddingLeft).isEqualTo(16)
            Truth.assertThat(buttonHelper.btnPaddingTop).isEqualTo(12)
            Truth.assertThat(buttonHelper.btnPaddingRight).isEqualTo(16)
            Truth.assertThat(buttonHelper.btnPaddingBottom).isEqualTo(12)
        }
    }

    //testForButtonType
    fun testGetTypePrimary() {
        var type:String = buttonHelper.primary
        Mockito.`when`(mockContext.getResources()).thenReturn(mockResources)
        Mockito.`when`(mockResources.getColor(R.color.blue_500)).thenReturn(R.color.blue_500)
        buttonHelper.getType(type, mockContext)

        Truth.assertThat(buttonHelper.labelButtonTextColor).isEqualTo(mockContext.resources.getColor(R.color.blue_500))
        Truth.assertThat(buttonHelper.leftColor).isEqualTo(mockContext.resources.getColor(R.color.blue_500))
        Truth.assertThat(buttonHelper.rightColor).isEqualTo(mockContext.resources.getColor(R.color.blue_500))
    }

    fun testGetTypeDanger(){
        var type:String = buttonHelper.danger
        Mockito.`when`(mockContext.getResources()).thenReturn(mockResources)
        Mockito.`when`(mockResources.getColor(R.color.red_500)).thenReturn(R.color.red_500)
        buttonHelper.getType(type, mockContext)

        Truth.assertThat(buttonHelper.labelButtonTextColor).isEqualTo(mockContext.resources.getColor(R.color.red_500))
        Truth.assertThat(buttonHelper.leftColor).isEqualTo(mockContext.resources.getColor(R.color.red_500))
        Truth.assertThat(buttonHelper.rightColor).isEqualTo(mockContext.resources.getColor(R.color.red_500))
    }

    fun testGetTypeWarning(){
        var type:String = buttonHelper.warning
        Mockito.`when`(mockContext.getResources()).thenReturn(mockResources)
        Mockito.`when`(mockResources.getColor(R.color.orange_brand_500)).thenReturn(R.color.orange_brand_500)
        buttonHelper.getType(type, mockContext)

        Truth.assertThat(buttonHelper.labelButtonTextColor).isEqualTo(mockContext.resources.getColor(R.color.orange_brand_500))
        Truth.assertThat(buttonHelper.leftColor).isEqualTo(mockContext.resources.getColor(R.color.orange_brand_500))
        Truth.assertThat(buttonHelper.rightColor).isEqualTo(mockContext.resources.getColor(R.color.orange_brand_500))
    }

    fun testGetTypeSuccess(){
        var type:String = buttonHelper.success
        Mockito.`when`(mockContext.getResources()).thenReturn(mockResources)
        Mockito.`when`(mockResources.getColor(R.color.green_500)).thenReturn(R.color.green_500)
        buttonHelper.getType(type, mockContext)

        Truth.assertThat(buttonHelper.labelButtonTextColor).isEqualTo(mockContext.resources.getColor(R.color.green_500))
        Truth.assertThat(buttonHelper.leftColor).isEqualTo(mockContext.resources.getColor(R.color.green_500))
        Truth.assertThat(buttonHelper.rightColor).isEqualTo(mockContext.resources.getColor(R.color.green_500))
    }

    fun testGetTypePrimaryWhite(){
        var type:String = buttonHelper.primaryWhite
        Mockito.`when`(mockContext.getResources()).thenReturn(mockResources)
        Mockito.`when`(mockResources.getColor(R.color.gray_50)).thenReturn(R.color.gray_50)
        buttonHelper.getType(type, mockContext)

        Truth.assertThat(buttonHelper.labelButtonTextColor).isEqualTo(mockContext.resources.getColor(R.color.gray_50))
        Truth.assertThat(buttonHelper.leftColor).isEqualTo(mockContext.resources.getColor(R.color.gray_50))
        Truth.assertThat(buttonHelper.rightColor).isEqualTo(mockContext.resources.getColor(R.color.gray_50))
    }

    fun testGetTypeInfo(){
        var type:String = buttonHelper.info
        Mockito.`when`(mockContext.getResources()).thenReturn(mockResources)
        Mockito.`when`(mockResources.getColor(R.color.cyan_500)).thenReturn(R.color.cyan_500)
        buttonHelper.getType(type, mockContext)

        Truth.assertThat(buttonHelper.labelButtonTextColor).isEqualTo(mockContext.resources.getColor(R.color.cyan_500))
        Truth.assertThat(buttonHelper.leftColor).isEqualTo(mockContext.resources.getColor(R.color.cyan_500))
        Truth.assertThat(buttonHelper.rightColor).isEqualTo(mockContext.resources.getColor(R.color.cyan_500))
    }

    fun testGetTypeLight(){
        var type:String = buttonHelper.light
        Mockito.`when`(mockContext.getResources()).thenReturn(mockResources)
        Mockito.`when`(mockResources.getColor(R.color.gray_700)).thenReturn(R.color.gray_700)
        buttonHelper.getType(type, mockContext)

        Truth.assertThat(buttonHelper.labelButtonTextColor).isEqualTo(mockContext.resources.getColor(R.color.gray_700))
        Truth.assertThat(buttonHelper.leftColor).isEqualTo(mockContext.resources.getColor(R.color.gray_700))
        Truth.assertThat(buttonHelper.rightColor).isEqualTo(mockContext.resources.getColor(R.color.gray_700))
    }

    //testForButtonDisableMode
    fun testGetDisableFalse() {
        var disable:String = buttonHelper.disableFalse
        var style:String = ""
        var type:String = ""
        buttonHelper.getDisable(disable,style,type,mockContext)
        Truth.assertThat(buttonHelper.btnLayoutIsEnable).isEqualTo(true)
    }

    fun testGetDisableTrue() {
        var disable:String = buttonHelper.disableTrue
        var style:String = ""
        var type:String = ""
        buttonHelper.getDisable(disable,style,type,mockContext)

        Truth.assertThat(buttonHelper.btnLayoutIsEnable).isEqualTo(false)
        if (style == buttonHelper.fill) {
            if (type == buttonHelper.primaryWhite) {
                Mockito.`when`(mockContext.getResources()).thenReturn(mockResources)
                Mockito.`when`(mockResources.getColor(R.color.gray_400)).thenReturn(R.color.gray_400)

                Truth.assertThat(buttonHelper.labelButtonTextColor).isEqualTo(mockContext.resources.getColor(R.color.gray_400))
                Truth.assertThat(buttonHelper.leftColor).isEqualTo(mockContext.resources.getColor(R.color.gray_400))
                Truth.assertThat(buttonHelper.rightColor).isEqualTo(mockContext.resources.getColor(R.color.gray_400))
            }
        }else if (style == buttonHelper.nude || style == buttonHelper.outline || style == buttonHelper.link){
            if (type == buttonHelper.primary){
                Mockito.`when`(mockContext.getResources()).thenReturn(mockResources)
                Mockito.`when`(mockResources.getColor(R.color.blue_100)).thenReturn(R.color.blue_100)

                Truth.assertThat(buttonHelper.labelButtonTextColor).isEqualTo(mockContext.resources.getColor(R.color.blue_100))
                Truth.assertThat(buttonHelper.leftColor).isEqualTo(mockContext.resources.getColor(R.color.blue_100))
                Truth.assertThat(buttonHelper.rightColor).isEqualTo(mockContext.resources.getColor(R.color.blue_100))
            } else if (type == buttonHelper.danger){
                Mockito.`when`(mockContext.getResources()).thenReturn(mockResources)
                Mockito.`when`(mockResources.getColor(R.color.red_100)).thenReturn(R.color.red_100)

                Truth.assertThat(buttonHelper.labelButtonTextColor).isEqualTo(mockContext.resources.getColor(R.color.red_100))
                Truth.assertThat(buttonHelper.leftColor).isEqualTo(mockContext.resources.getColor(R.color.red_100))
                Truth.assertThat(buttonHelper.rightColor).isEqualTo(mockContext.resources.getColor(R.color.red_100))
            } else if (type == buttonHelper.warning){
                Mockito.`when`(mockContext.getResources()).thenReturn(mockResources)
                Mockito.`when`(mockResources.getColor(R.color.orange_brand_100)).thenReturn(R.color.orange_brand_100)

                Truth.assertThat(buttonHelper.labelButtonTextColor).isEqualTo(mockContext.resources.getColor(R.color.orange_brand_100))
                Truth.assertThat(buttonHelper.leftColor).isEqualTo(mockContext.resources.getColor(R.color.orange_brand_100))
                Truth.assertThat(buttonHelper.rightColor).isEqualTo(mockContext.resources.getColor(R.color.orange_brand_100))
            } else if (type == buttonHelper.success){
                Mockito.`when`(mockContext.getResources()).thenReturn(mockResources)
                Mockito.`when`(mockResources.getColor(R.color.green_100)).thenReturn(R.color.green_100)

                Truth.assertThat(buttonHelper.labelButtonTextColor).isEqualTo(mockContext.resources.getColor(R.color.green_100))
                Truth.assertThat(buttonHelper.leftColor).isEqualTo(mockContext.resources.getColor(R.color.green_100))
                Truth.assertThat(buttonHelper.rightColor).isEqualTo(mockContext.resources.getColor(R.color.green_100))
            } else if (type == buttonHelper.primaryWhite){
                Mockito.`when`(mockContext.getResources()).thenReturn(mockResources)
                Mockito.`when`(mockResources.getColor(R.color.gray_400)).thenReturn(R.color.gray_400)

                Truth.assertThat(buttonHelper.labelButtonTextColor).isEqualTo(mockContext.resources.getColor(R.color.gray_400))
                Truth.assertThat(buttonHelper.leftColor).isEqualTo(mockContext.resources.getColor(R.color.gray_400))
                Truth.assertThat(buttonHelper.rightColor).isEqualTo(mockContext.resources.getColor(R.color.gray_400))
            } else if (type == buttonHelper.info){
                Mockito.`when`(mockContext.getResources()).thenReturn(mockResources)
                Mockito.`when`(mockResources.getColor(R.color.cyan_100)).thenReturn(R.color.cyan_100)

                Truth.assertThat(buttonHelper.labelButtonTextColor).isEqualTo(mockContext.resources.getColor(R.color.cyan_100))
                Truth.assertThat(buttonHelper.leftColor).isEqualTo(mockContext.resources.getColor(R.color.cyan_100))
                Truth.assertThat(buttonHelper.rightColor).isEqualTo(mockContext.resources.getColor(R.color.cyan_100))
            } else if (type == buttonHelper.light){
                Mockito.`when`(mockContext.getResources()).thenReturn(mockResources)
                Mockito.`when`(mockResources.getColor(R.color.gray_300)).thenReturn(R.color.gray_300)

                Truth.assertThat(buttonHelper.labelButtonTextColor).isEqualTo(mockContext.resources.getColor(R.color.gray_300))
                Truth.assertThat(buttonHelper.leftColor).isEqualTo(mockContext.resources.getColor(R.color.gray_300))
                Truth.assertThat(buttonHelper.rightColor).isEqualTo(mockContext.resources.getColor(R.color.gray_300))
            }
        }
    }

    //testForButtonStyle
    fun testGetStyleFillButton() {
        var style:String = buttonHelper.fill
        var type:String = ""

        buttonHelper.getStyle(style,mockContext, type)
        if (type == buttonHelper.primaryWhite){
            Truth.assertThat(buttonHelper.btnLayoutColor).isEqualTo(R.drawable.ripple_fill_button_primary_white)
            Truth.assertThat(buttonHelper.progressBarBtnColor).isEqualTo(R.drawable.progress_bar_button_primary)

            Mockito.`when`(mockContext.getResources()).thenReturn(mockResources)
            Mockito.`when`(mockResources.getColor(R.color.blue_500)).thenReturn(R.color.blue_500)
            Truth.assertThat(buttonHelper.labelButtonTextColor).isEqualTo(mockContext.resources.getColor(R.color.blue_500))
            Truth.assertThat(buttonHelper.leftColor).isEqualTo(mockContext.resources.getColor(R.color.blue_500))
            Truth.assertThat(buttonHelper.rightColor).isEqualTo(mockContext.resources.getColor(R.color.blue_500))
        }else if (type == buttonHelper.primary || type == buttonHelper.danger || type == buttonHelper.warning || type == buttonHelper.success|| type ==buttonHelper.info|| type == buttonHelper.light){
            Truth.assertThat(buttonHelper.progressBarBtnColor).isEqualTo(R.drawable.progress_bar_button_white)

            Mockito.`when`(mockContext.getResources()).thenReturn(mockResources)
            Mockito.`when`(mockResources.getColor(R.color.gray_50)).thenReturn(R.color.gray_50)
            Truth.assertThat(buttonHelper.labelButtonTextColor).isEqualTo(mockContext.resources.getColor(R.color.gray_50))
            Truth.assertThat(buttonHelper.leftColor).isEqualTo(mockContext.resources.getColor(R.color.gray_50))
            Truth.assertThat(buttonHelper.rightColor).isEqualTo(mockContext.resources.getColor(R.color.gray_50))
            if (type == buttonHelper.primary){
                Truth.assertThat(buttonHelper.btnLayoutColor).isEqualTo(R.drawable.ripple_fill_button_primary)
            } else if (type == buttonHelper.danger){
                Truth.assertThat(buttonHelper.btnLayoutColor).isEqualTo(R.drawable.ripple_fill_button_danger)
            } else if (type == buttonHelper.warning){
                Truth.assertThat(buttonHelper.btnLayoutColor).isEqualTo(R.drawable.ripple_fill_button_warning)
            } else if (type == buttonHelper.success){
                Truth.assertThat(buttonHelper.btnLayoutColor).isEqualTo(R.drawable.ripple_fill_button_success)
            } else if (type == buttonHelper.info){
                Truth.assertThat(buttonHelper.btnLayoutColor).isEqualTo(R.drawable.ripple_fill_button_info)
            } else if (type == buttonHelper.light){
                Truth.assertThat(buttonHelper.btnLayoutColor).isEqualTo(R.drawable.ripple_fill_button_light)
            }
        }
    }

    fun testGetStyleOutlineButton() {
        var style:String = buttonHelper.outline
        var type:String = ""
        buttonHelper.getStyle(style,mockContext, type)

        if (type == buttonHelper.primary){
            Truth.assertThat(buttonHelper.btnLayoutColor).isEqualTo(R.drawable.ripple_outline_button_primary)
            Truth.assertThat(buttonHelper.progressBarBtnColor).isEqualTo(R.drawable.progress_bar_button_primary)
        } else if (type == buttonHelper.warning){
            Truth.assertThat(buttonHelper.btnLayoutColor).isEqualTo(R.drawable.ripple_outline_button_warning)
            Truth.assertThat(buttonHelper.progressBarBtnColor).isEqualTo(R.drawable.progress_bar_button_warning)
        } else if (type == buttonHelper.danger){
            Truth.assertThat(buttonHelper.btnLayoutColor).isEqualTo(R.drawable.ripple_outline_button_danger)
            Truth.assertThat(buttonHelper.progressBarBtnColor).isEqualTo(R.drawable.progress_bar_button_danger)
        } else if (type == buttonHelper.success){
            Truth.assertThat(buttonHelper.btnLayoutColor).isEqualTo(R.drawable.ripple_outline_button_success)
            Truth.assertThat(buttonHelper.progressBarBtnColor).isEqualTo(R.drawable.progress_bar_button_success)
        } else if (type == buttonHelper.primaryWhite){
            Truth.assertThat(buttonHelper.btnLayoutColor).isEqualTo(R.drawable.ripple_outline_button_primary_white)
            Truth.assertThat(buttonHelper.progressBarBtnColor).isEqualTo(R.drawable.progress_bar_button_white)
        } else if (type == buttonHelper.info){
            Truth.assertThat(buttonHelper.btnLayoutColor).isEqualTo(R.drawable.ripple_outline_button_info)
            Truth.assertThat(buttonHelper.progressBarBtnColor).isEqualTo(R.drawable.progress_bar_button_info)
        } else if (type == buttonHelper.light){
            Truth.assertThat(buttonHelper.btnLayoutColor).isEqualTo(R.drawable.ripple_outline_button_light)
            Truth.assertThat(buttonHelper.progressBarBtnColor).isEqualTo(R.drawable.progress_bar_button_light)
        }
    }

    fun testGetStyleNudeButton() {
        var style:String = buttonHelper.nude
        var type:String = ""

        buttonHelper.getStyle(style,mockContext, type)

        if (type == buttonHelper.primary){
            Truth.assertThat(buttonHelper.progressBarBtnColor).isEqualTo(R.drawable.progress_bar_button_primary)
        } else if (type == buttonHelper.warning){
            Truth.assertThat(buttonHelper.progressBarBtnColor).isEqualTo(R.drawable.progress_bar_button_warning)
        } else if (type == buttonHelper.danger){
            Truth.assertThat(buttonHelper.progressBarBtnColor).isEqualTo(R.drawable.progress_bar_button_danger)
        } else if (type == buttonHelper.success){
            Truth.assertThat(buttonHelper.progressBarBtnColor).isEqualTo(R.drawable.progress_bar_button_success)
        } else if (type == buttonHelper.primaryWhite){
            Truth.assertThat(buttonHelper.progressBarBtnColor).isEqualTo(R.drawable.progress_bar_button_white)
        } else if (type == buttonHelper.info){
            Truth.assertThat(buttonHelper.progressBarBtnColor).isEqualTo(R.drawable.progress_bar_button_info)
        } else if (type == buttonHelper.light){
            Truth.assertThat(buttonHelper.progressBarBtnColor).isEqualTo(R.drawable.progress_bar_button_light)
        }
    }

    fun testGetStyleLinkButton() {
        var style:String = buttonHelper.link
        var type:String = ""
        buttonHelper.getStyle(style,mockContext,type)
        if (type == buttonHelper.primary){
            Truth.assertThat(buttonHelper.progressBarBtnColor).isEqualTo(R.drawable.progress_bar_button_primary)
        } else if (type == buttonHelper.warning){
            Truth.assertThat(buttonHelper.progressBarBtnColor).isEqualTo(R.drawable.progress_bar_button_warning)
        } else if (type == buttonHelper.danger){
            Truth.assertThat(buttonHelper.progressBarBtnColor).isEqualTo(R.drawable.progress_bar_button_danger)
        } else if (type == buttonHelper.success){
            Truth.assertThat(buttonHelper.progressBarBtnColor).isEqualTo(R.drawable.progress_bar_button_success)
        } else if (type == buttonHelper.primaryWhite){
            Truth.assertThat(buttonHelper.progressBarBtnColor).isEqualTo(R.drawable.progress_bar_button_white)
        } else if (type == buttonHelper.info){
            Truth.assertThat(buttonHelper.progressBarBtnColor).isEqualTo(R.drawable.progress_bar_button_info)
        } else if (type == buttonHelper.light){
            Truth.assertThat(buttonHelper.progressBarBtnColor).isEqualTo(R.drawable.progress_bar_button_light)
        }
    }
}